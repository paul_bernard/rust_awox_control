#[allow(unused_attributes)]
#[allow(unused_imports)]
#[allow(unused_assignments)]

use btleplug::api::bleuuid::BleUuid;
use btleplug::api::{Central, CentralEvent, Manager as _, Peripheral as _, ScanFilter, Characteristic, WriteType};
use btleplug::platform::{Adapter, Manager, Peripheral, PeripheralId};
use crypto::symmetriccipher::BlockEncryptor;
use std::collections::BTreeSet;
use futures::stream::StreamExt;
use uuid::{self, Uuid};

use rand::{Rng, thread_rng};
use env_logger;

use crypto::aessafe;

use std::thread;
use std::io;
use std::io::Write;

struct Session {
    light: Peripheral,
    mac: [u8; 6],
    name: [u8; 16],
    password: [u8; 16],
    session_key: [u8; 16],
    packet_id: [u8; 3],
    pair_char_uuid: Uuid,
    command_char_uuid: Uuid,
    status_char_uuid: Uuid,
    ota_char_uuid: Uuid,
}

impl Session {
    async fn new(
        light : Peripheral,
        name : String,
        password : String) -> Session {

        let pair_char_uuid : Uuid= Uuid::parse_str("00010203-0405-0607-0809-0a0b0c0d1914").unwrap();
        let command_char_uuid : Uuid = Uuid::parse_str("00010203-0405-0607-0809-0a0b0c0d1912").unwrap();
        let status_char_uuid : Uuid = Uuid::parse_str("00010203-0405-0607-0809-0a0b0c0d1911").unwrap();
        let ota_char_uuid : Uuid = Uuid::parse_str("00010203-0405-0607-0809-0a0b0c0d1913").unwrap();
        
        let mac = light.address().into_inner();

        let name_slice = create_ljust_slice(name);
        let password_slice = create_ljust_slice(password);
        
        let session_random = thread_rng().gen::<[u8; 8]>();
        //let session_random = [1u8, 2, 3, 4, 5, 6, 7, 8];
        
        thread::sleep(std::time::Duration::from_millis(500));
        let characteristics : BTreeSet<Characteristic> = light.characteristics();
        let pair_char = characteristics
            .iter()
            .find(|c| c.uuid == pair_char_uuid)
            .unwrap();
        let status_char = characteristics
            .iter()
            .find(|c| c.uuid == status_char_uuid)
            .unwrap();
        
        let pair_data = Session::make_random_request(&name_slice,&password_slice, &session_random);
        
        //println!("session random : {:?}",&session_random);
        //println!("pair data : {:?}",&pair_data);

        light.write(pair_char, &pair_data[..], WriteType::WithResponse)
            .await
            .unwrap();
        
        light.write(status_char, &[0x01], WriteType::WithoutResponse)
            .await
            .unwrap();

        let pair_reply = light.read(pair_char)
            .await
            .unwrap();
        
        let session_key = Session::create_session_key(&name_slice, &password_slice, &session_random, pair_reply);

        let packet_id = [0u8; 3];
        Session {
            light,
            mac,
            name: name_slice,
            password: password_slice,
            session_key,
            packet_id,
            pair_char_uuid,
            command_char_uuid,
            status_char_uuid,
            ota_char_uuid,
        }
    }

    fn update_packet_id(&mut self) {
        //println!("update_packet_id");
        if self.packet_id[2] == 255 {
            if self.packet_id[1] == 255 {
                self.packet_id[0] += 1;
                self.packet_id[1] = 0;
            } else {
                self.packet_id[1] += 1;
            }
            self.packet_id[2] = 0;
        } else {
            self.packet_id[2] += 1;
        }
    }

    async fn set_color(&mut self, color: [u8; 3]) {
        //println!("set_color");
        // data taille de 4
        let mut data = vec![0x04, color[0],color[1],color[2]];
        self.write_command(0xE2,&mut data).await;
    }

    async fn write_command(&mut self, code : u8, data : &mut Vec<u8>) {
        //println!("write_command");
        let destination = 0u8; // mesh_id, à voir
        // make command packet
        self.update_packet_id();

        // Get command characteristic
        // Build nonce
        let mut mac = self.mac.clone();
        mac.reverse();
        let nonce = &[&mac[0..4], &[0x01][..], &self.packet_id[..]].concat()[..];
        // Build payload
        // Taille de 15 si data assez petit
        let mut payload = vec![destination, 0x00];
        payload.extend_from_slice(&[code, 0x60, 0x01]);
        payload.append(data);
        payload.resize_with(15, || 0x00);
        // Build checksum
        let check = self.make_checksum(nonce,&payload);
        // Crypt payload
        let encrypted_payload = self.encrypt_payload(nonce,payload);
        // Create packet data
        let packet_data = &[&self.packet_id[..], &check[..2], &encrypted_payload[..]].concat()[..];

        let characteristics: BTreeSet<Characteristic> = self.light.characteristics();
        let command_char = characteristics
            .iter()
            .find(|c| c.uuid == self.command_char_uuid)
            .unwrap();
        //println!("write_command Packet data : {:?}",&packet_data);
        // Write command
        self.light.write(command_char, packet_data, WriteType::WithoutResponse)
            .await
            .unwrap();
    }

    fn make_checksum(&self, nonce : &[u8],payload : &Vec<u8>) -> [u8; 16] {
        //println!("make_checksum");
        assert!(nonce.len() == 8);
        // Si panique, voir packetutils.py
        assert!(payload.len() <= 16);
        let mut base = [0u8; 16];
        base[..8].copy_from_slice(nonce);
        base[8] = payload.len() as u8;
        let check = Session::encrypt(&self.session_key, &base);
        let mut payload_array = [0u8; 16];
        payload_array[..payload.len()].copy_from_slice(&payload[..]);
        let check2 = bit_xor(&check, &payload_array);

        let val_retour = Session::encrypt(&self.session_key, &check2);
        //println!("encrypt check2 : {:?}",val_retour);
        val_retour
    }

    fn encrypt_payload(&self, nonce : &[u8],payload : Vec<u8>) -> Vec<u8> {
        //println!("encrypt_payload");
        assert!(nonce.len() <= 15);
        assert!(payload.len() <= 16);

        let mut base = [0u8; 16];
        base[1..(nonce.len()+1)].copy_from_slice(nonce);

        let mut payload_array = [0u8; 16];
        payload_array[..payload.len()].copy_from_slice(&payload[..]);

        let encrypted_base = Session::encrypt(&self.session_key, &base);
        bit_xor(&encrypted_base, &payload_array)[..payload.len()].to_vec()
    }

    fn create_session_key(name: &[u8; 16], password: &[u8; 16], random: &[u8; 8], reply: Vec<u8>) -> [u8; 16] {
        //println!("create_session_key");
        assert!(reply[0] == 0xd);
        let mut value: [u8; 16] = Default::default();
        value[..8].copy_from_slice(random);
        value[8..].copy_from_slice(&reply[1..9]);

        let name_password_xor = bit_xor(name, password);
        let key = Session::encrypt(&name_password_xor, &value);

        key
    }

    fn make_random_request(name : &[u8; 16], password : &[u8; 16], random : &[u8; 8]) -> Vec<u8> {
        //println!("make_random_request");
        let name_password_xor = bit_xor(name, password);
        let mut random_slice : [u8; 16] = [0u8;16];
        random_slice[..8].copy_from_slice(&random[..]);

        let enc = Session::encrypt(&random_slice,&name_password_xor);
        let entete : &[u8] = &[0x0c];
        let packet_data = [entete, random, &enc[..8]].concat();

        packet_data
    }

    fn encrypt(key : &[u8; 16], value: &[u8; 16]) -> [u8; 16] {
        let mut rev_key = key.clone();
        rev_key.reverse();
        let mut rev_value = value.clone();
        rev_value.reverse();
        let mut output = [0u8;16];
        
        let cipher = aessafe::AesSafe128Encryptor::new(&rev_key);
        cipher.encrypt_block(&rev_value,&mut output);

        output.reverse();
        output
    }

}


#[tokio::main]
async fn main() {
    env_logger::init();
    let manager = Manager::new().await.unwrap();

    let central = get_central(&manager).await;
    let mut events = central.events().await.unwrap();

    central.start_scan(ScanFilter::default()).await.unwrap();

    let light : Peripheral;

    while let Some(event) = events.next().await {
        match event {
            CentralEvent::DeviceDiscovered(id) => {
                println!("DeviceDiscovered: {:?}", id);

                if let Some(light) = is_awox(&central, &id).await {
                    println!("Tentative de connexion avec {:?}",id);
                    let _ = light.connect().await.unwrap();
                }
                    
            }
            CentralEvent::DeviceConnected(id) => {
                println!("DeviceConnected: {:?}", id);
                let light = central.peripheral(&id).await.unwrap();
                let _ = start_connection(light).await;

                let _ = central.stop_scan().await.unwrap();
                break;
                
            }
            CentralEvent::DeviceDisconnected(id) => {
                println!("DeviceDisconnected: {:?}", id);
            }
            
            _ => {}
        }
    }
}

async fn start_connection(light : Peripheral) {
    
    light.discover_services().await.unwrap();

    let mut session = Session::new(light, String::from("paul"), String::from("aaaa")).await;
    
    let mut buffer = String::new();
    let stdin = io::stdin();
    let mut stdout = io::stdout();
    loop {
        print!("Please enter your input:\n> ");
        stdout.flush().unwrap();
        stdin.read_line(&mut buffer)
            .unwrap();
        if buffer == String::from("exit\n") { break };
        parse_message(&buffer, &mut session).await;
        buffer.clear();
    }
}

async fn parse_message(message : &String, light : &mut Session) {
    //println!("Parse message");
    let color : Vec<u8> = message[..message.len()-1]
        .split_whitespace()
        .map(|arg| {
            arg.parse::<u8>().unwrap()
        })
        .collect();
    
    if color.len() == 3 {
        //println!("Commande bien interprétée, changement de couleur...");
        light.set_color([color[0], color[1], color[2]]).await;
    }
    else {
        //println!("Nombre anormal de couleur...? : {:?}",message);
    }
}

async fn get_central(manager: &Manager) -> Adapter {
    let adapters = manager.adapters().await.unwrap();
    adapters.into_iter().nth(0).unwrap()
}


async fn is_awox(central: &Adapter, id : &PeripheralId) -> Option<Peripheral> {
    let p = central.peripheral(id).await.unwrap();
    let properties = p.properties()
        .await
        .unwrap()
        .unwrap();
    if properties
        .manufacturer_data
        .contains_key(&352) {
            //println!("{:?}",properties);
            return Some(p);
        }
    None
}

fn create_ljust_slice(name : String) -> [u8; 16] {
    assert!(name.len() <= 16, "name and password length must be <= 16");
    let mut sized_ljust_slice = [0u8; 16];
    sized_ljust_slice[..name.as_bytes().len()].copy_from_slice(&name.as_bytes()[..]);
    sized_ljust_slice
}

fn bit_xor(slice1 : &[u8; 16], slice2 : &[u8; 16]) -> [u8; 16] {
    let mut xor_slice = [0u8; 16];

    for i in 0..16 {
        xor_slice[i] = slice1[i] ^ slice2[i];
    }

    xor_slice
} 